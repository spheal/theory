## High availability
Maintaining acceptable continious perfomance despite of the temprorary load or failure in service or hardware.  
## Fault tolerance
System's ability to continue operating when one oe more ir's components fails.  
## Disaster recovery
System's ability to back up and restore data/apps/resources when needed.  
## Scalability
Ability to increase the instance count or size of existing resources.  
## Elasticity
Ability to increase or decrease the instance count or size of existing resources based on fluctuating traffic or load.  
## Active directory
Service that acts as a store of user credentials and account information in your environment.  
## Subscription model 
![alt](https://linuxacademy.com/site-content/uploads/2017/06/user_60255_594cb0037a634.PNG_800.jpg)   
## CapEx vs OpEx
In cloud:
**CapEx** you are paying up-front for all assets of data center. Advantage for a long period projects.  
**OpEx** you are paying for what you used. 
## Consumption-based model
Consumption-based model is a model where you are paying only for the resource you used. Comparing to a usual model where you are reserving resources or buying servers it's more flexible: your servers aren't idle and you won't get into situation where your servers can't handle load and providing new servers takes time.  
## Economies of scale
The more amount of product you are buying the less price you pay for a commodity unit.Cloud providers have plenty of datacenters where they buy lot of different equipment(they get discounts) -> overall price for building datacenter/buying hardware and etc will be significantly lower comparing to a small company.  
#### Ways to work with cloud
- CLI
- Powershell cmdlets
- Cloudshell
- Portal web interface
#### Advisor
Service that analyze your cloud usage and give advice how to improve HA,perfomance,cost and other params. It provides best practices on how to use Azure.
## XaaS
XaaS represents the way in which resource are given to the end user and a level of control end user has to these resources.
![alt](https://carlfreemandotnet.files.wordpress.com/2017/02/azure-on-premises-vs-iaas-vs-paas-vs-saas.png?w=1024)  
## Types of cloud
#### Public cloud
Type of cloud where hardware owned and operated by Service Provider. Also this type of cloud is multitenant,means that multiple customers use the same resources.  
Pros:
+ No upfront CapEx
+ No maintenance
+ Highly reliable
+ Easy scalling

Cons:
- Less customizable
- Potential latency
- Problems with governance

Good for:
1. New cloud-oriented application 
2. Webapp,SQL database and etc.
3. Don't need much customization
#### Private cloud
Type of cloud where hardware owned and operated by IT organisation. It has single tenant implementation.  
Pros:  
+ Full controll
+ Better perfomance
+ Higher lavel of security

Cons:
- Hight upfront CapEx
- Maintenance
- Possibility of under-utilization of resources

Good for:
1. Governance restrictions
2. Enhanced security
#### Hybrid cloud
Combination of public and private cloud resources.  
Pros:
+ Flexibility
+ Resistance to outage
+ Manageable security

Cons:
- High upfront CapEx
- Risk of resource under-utilization
- A lot maintenance

Good for:
1. Secure applications/services
2. System that need some customization
## Azure concepts
#### Region
A region is a set of datacenters deployed within a latency-defined perimeter(ex: withtin 500km) and connected through a dedicated regional low-latency network.  
<pre>
Multiregional failover: ----------> Active Datacenter  
                              |  
                              ----> Passive Datacenter
</pre>

<pre>
Multiregional deployment: --------> Active Datacenter  
                              |  
                              ----> Active Datacenter
</pre>

#### Availability Zones
AZ is a unique physical location within Azure region made up of one or multiple datacenters. Provide HA by protecting from datacenters outages.  
#### Resource groups
RG is related Azure services grouped together. Resources aimed for the same goal. RG is not tied to specific region. Can apply policies to SG. This is only logical separation.  
#### Azure resourse manager
Azure resource Manger(ARM) is a deployment and management service that provides an ability to create/update/delete/manage resources in your subscription including templates. Also provides and ability to use access control,auditing,tagging to secure and organize resources after deployment.  
#### Network security code(NSG)
NSG is a collection of network security rules that defines how traffic will ago ex/internally.
#### Active Directory(AD)
AD is a highly avaliable,fully managed Identity and Access management(IAM) cloud solution. Operation unit is a user. Users can be gathred in groups. Every user is assigned to a licence. Premium licence provides additional features.
#### AD sync
Service for synchronizing on premise AD server with the cloud-based AD service.
#### Security center
Service that protects Azure and non-Azure resources by collecting events from Linux/Windows OS and analyzing them in security analytics center. This is done by installing speacial agent on every node. In Azure PaaS solutions this is already deployed.
#### Key vault
Service for centralized secrets storing. Instead of storing passwords in plain text key application will use URI to get a sensetive information.
#### Information protection(AIP)
Service that helps to classify and protect documents and emails by applying labels to them. This classification can be done auto/manually.Based on these labels you can add additional security level to distributing information.  
#### Advanced threat protection(ATP)
Solution that leverages your on-premises Active Directory signals to identify, detect, and investigate advanced threats, compromised identities, and malicious insider actions directed at your organization.
#### Polices
Service that allows you to create/manage/assing policies to subscriptions/resource groups/management groups. E.g size of vms,resource locationm,apply default tags,require specific version of software. 
#### Monitoring
Platform fot monitoring all azure resources.Helps to understand how your applications are performing and proactively identify issues affecting them and resources they depend on.
#### Service health
Service that provides a personalized view of healt of the Azure services and regions you are using.
3 types of events that can impact your resources:  
- Service issue
- Planned maintenance 
- Health advisores(changes in azure products that require your attention. E.g deprecating a feature in one product,exceed a usage qouta)
#### Initiatives
Set of policies that allows you to combine different policies as one object and apply it to one or multiple subscriptions.
#### Resourse locking
Prevent users from accidential deleting/modifying critical resources.Applyed to Management groups,Subscriptions,Resource group,Resources.
## Core products and services
### Computing services
#### Virtal machine(IaaS)
Service that provides on prem VMs with the ability to configure them(RAM,CPU,Disk).
#### Virtal machine scale set(IaaS)
Pool of VMs that scales up or down based on traffic load. All vms use the same params as OS,RAM and etc. Also this service creates LB to distribute traffic among pool.
#### App service(PaaS)
Platform to build and host web application,mobile backend and API. No infrastructure management. Autoscalling and HA.
### Network services
#### Virtal network
Service that provides an ability to create and manage virtual networks. Also set the rules how your resources will communicate with Internet,other services or how they can be reachable.
#### VPN Gateway
Service that provides an ability to securely and privately connect on premise network with Azure virtual network or 2 Azure virtial network.
#### Load balancer
Service that provides an ability to balance traffic between some resources.
#### App Gateway
Service that provides some advanced feateres to ALB , like balancing based on URL. Also you can customize it accroding to your needs. Also you can encrypt your traffic.
#### Content delivery network
Service that can host static content at a variety of DC. CDN delivers static content from the closest DC to user location that made the request.
#### Firewall
Managed cloud-based security service that protects virual network resources by applying connectivity policies. Fully integrated with Azure Monitor(logs).
#### DDoS Protection
Service that can help you to mitigate some types of DDoS attacks in your virtual network.  
Typers of DDoS attacks: 
- Volumetric attacks. Flood the network with great amount of seemingly legitimate traffic.
- Protocol attacks. Render a target inaccessability by exploting weaknesses in the layer 3/4.
- Resource (application) layer attacks. Disrupt the transmission of data between hosts.
### Storage services
#### Disks Storage(IaaS)
Service that provides disks for VMs. Premium disk == ssd.
##### Types of storage
*Hot tier* provides instance access to the data and has highest price. Intended for frequently used data.  
*Cold tier* takes more time to get data(!10-100 ms).Intended for data  that is infrequently accessed and stored for at least 30 days.  
*Archive tier* it can take up 4 hours from data request and till you get it. Intended for rarely used data.  
#### File(IaaS)
Service that provides simple,secure and fully managed cloud file share.  
Good for:
- Logs,metrics,crash dumps  
- Config files  
#### Blob(PaaS)
Service that provides blob storage to store differnt obects. 
Good for:
- Images or documents
- Files for distributed access
- Back ups,archives,DR
- Data for analysis
- Streaming video and audio
#### Table(PaaS)
Service that provides NoSQL database for storing data as a key:value objects. This service provides more structured way of file storing(ex: you can query files based on file extension).
Good for:
- Storing TBs of structured data capable of serving web scale applications
- Storing datasets that don't require complex joins, foreign keys, or stored procedures and can be denormalized for fast access
#### Queue(PaaS)
Service to store and retrieve messages. 

### Database services
Service that provides storage for large number of messages.It can be used for apps communication. 
#### Cosmos DB(PaaS)
Service that provides globally distributed multimodel(multiple ways to query data: SQK,MongoDB,Cassandra and etc.) NoSQL database. Also it supports analytcs through Apache Spark. 
#### SQL database(IaaS or PaaS)
IaaS: SQL server on AVM. Install manually or use prebuild images.
PaaS: Fully managed SQL database engine. Need only to configure database.
#### SQL data warehouse(PaaS)
SQL database designed for huge amount of data that can continue growing. Quckly searchung for information by splitting  huge task in smaller ones and executing them in parallel.
#### Database migration Service
Service for migration on-premise databases from different resources in Azure Database. 
#### Market
Serive that provides an online store that offers applications and services either built on or designed to integrate with Microsoft's Azure public cloud.
### IoT services
#### IoT Central(SaaS)
Fully managed solution that provides an ability fastly connect IoT devices,manage them,manage data through the web interface. It is working on top of IoT Hub. 
#### IoT Hub(PaaS)
Core messaging service
IoT Hub is managed service to enable bi-directional communication between IoT devices and Azure
### Big data services 
#### HDInsights
Open source framework for distributed processing and analysis of big data sets on clusters. Based on Hadoop framework.
#### Data lake analytics(PaaS)
On demand analytcs job service. Idea is the same as In HDInsight,but this is more managed service and you don't need to configure Hadoop cluster like in HDInsights. Instant scalling,on-premise quering. Pay per job. You can create jobs using web portal and view the results. 
### Machine learning services
#### ML service(PaaS)
Cloud based environment that we can use to prepare our data,train or deploy models across Azure. You can integrate other open-source ML models. Good in case where you need to control algorithms and want to use other open-source libraries.
#### ML Studio(SaaS)
Web based solution where you can build,test,deploy ML solution without any code. Workflow will be defined in drug and drop way.
### Serverless services
#### Functions(Serverless)
Service that provides a way to define a trigger that will execute your code(function). Everything is managed by SP. You only pay for time you function was executed.
#### Logic Apps(Serverless)
Web based service that provide an ability to create complicated workflows using Azure functions. You can intgrate third-party services in your data workflow what makes service flexible.
#### Event grid(Serverless)
Event grid is a event routing service. It provides a way for different applications to communicate with each other. Events distributed across whole system as messages,that should be processed.




## Bigdata flow 
![alt](https://res.infoq.com/articles/azure-data-lake-analytics-usql/en/resources/azure-data-2-1568098316593.jpg)

## ML flow
![alt](https://msdnshared.blob.core.windows.net/media/2017/03/image423.png)

## Terms
#### Big data
*Big data* algorithms that provides you an ability to query data from large amounts of data and gain some insights based on it.
#### Machine learning
*ML* is a datascience technique thah allow compute to use existing data to make predictions for future behavious based on this data.
*Model* is a package that represnts consolidation of the rules that ML program figured out.