## Basic terms
**YAML** is a text based data serialization format that allows you to store data in hierarchy.  
**Data serialization** is the process of converting data objects present in complex data structures to byte stream.
## Elements of language
YAML oriented on Unicode but doesn't support C0/C1 blocks.Exceptions: tabs,new line,carriage return,deletes. Also doesn't support surrogates(U+D800). Restriction on UTF supporting. Allowed are UTF-8/16/32. UTF 32 is compatiable with JSON.
#### Styles
##### Block style
```
host: pc1
datacenter:
  location: GB
  cab: 212
roles:
  - web
  - dns
```
##### Flow style
Extention of JSON style
```
host: "pc1"
datacenter: { location: GB, cab: 212
roles: [ web, dns ]
```
#### Mappings(hash/dictionary)
Mapping is unordered collection of key value pairs.
```
host: pc1
datacenter:
  location: GB
  cab: 212
```
#### Sequences(list)
Sequence is a collection of values related to the same thing.
```
roles:
  - web
  - dns
```
#### Scalars
Scalar is basic element that make up a key or value. Besides basic values like one-line value or number you can put multiline strings. There two ways to set multiline values:
*With newline preservation*
```
downtime_sch: |
  2018-10-12 - kernal update
  2019-12-12 - security fix
```
*Without newline preservation*. All comment will show as one-line string.  
```
comment: >
  Making upgrade of 
  operating system
  as new bug was found.
```
#### Structure
--- means the beginning of the document.  
... means the end of the file.  
But these elements are optional to specify in most cases.
#### Comments
*#* symbol is used for comments.
#### Tags
Tags can work in 3 different ways:  
1. Change the way how YAML parser reads certain scalars.  
```
cab: !!str 13 
```
Default data types:  
  seq - Sequence  
  map - Map  
  str - String  
  int - Integer  
  float - Floating-point decimal  
  null - Null  
  binary - Binary code
  omap - Ordered map  
  set - Unordered set  
2. Set universal resource indicator
```
%TAG ! tag:hostdata:phl:
---
location: GB
```
3. Assign local tags to that indicator
```
datacenter:
  location: !PHL GB
```
#### Anchors
Anchors allow to reuse data over YAML file.
```
host: pc1
datacenter:
  location: GB
  cab: 212
roles: &host_roles
  - web
  - dns
---
host: pc2
datacenter:
  location: GB
  cab: 213
roles: *host_roles
```