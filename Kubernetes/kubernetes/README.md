## Basics
#### Kubernetes architecture 
![alt text](https://d33wubrfki0l68.cloudfront.net/518e18713c865fe67a5f23fc64260806d72b38f5/61d75/images/docs/post-ccm-arch.png)
#### Pod
**Pod** is one or several containers that share the same **network and storage**.
#### Service
**Service** is an abstraction that reprsents set of pods(e.g using selectors) and policies(e.g externally using LoadBalancer service type) by which we can access them.
#### Deployment
**Deployment** is a declarative(i.e you only specify what you want,without specifying how to it) definetion of the state for pods  and statefull sets.
#### Statefull Set
*Statefull set*
#### Label
**Label** is a key:value pair that attached to the object(e.g Pod). It helps to identify  and group objects among other objects in cluster.  
#### Selector
**Selector** is an abstraction(?) that provides opportunity to group objects.(e.g Group objects that have the same labels)
#### Job
**Job** is an object that creates pods to accomplish task specified number of times.
#### Controller
**Controller** is a loop process that is responsinle for a particular resource.(e.g Replication controller ensure number of replicas that specified in deployment)
#### Control Plane
**Control Plane** is components of Kubernetes that manages how Kubernetes communicates with the cluster.(e.g master VM, API server, scheduler, controller manager, cluster CA, root-of-trust key material and etc.) 
#### Endpoint 
**Endpoint** is an object that ties pods with specified labels to service.
#### Service account
**Service acoount** is an account that is used to interact with kube-api from pod.
#### Namespace
**Namespace** object that allows to split cluster on multiple virtual clusters. It becomes handy if multiplepeople/departments working on the same cluster.

## Worth to remember
Volume is attached to a **pod**.
#### Headless service
**Headless service** is a type of service that doesn't provide load balancing or proxying features, instead it gives an opportunity to communicate with the pod straightforward.  
#### Pod lifecycle
**Pending** - pod defined but not yet created.(e.g *Pulling container image or not enough resources to create a pod*)  
**Running** - pod attached to a node and all container have been created.  
**Succeeded** - All containers finished successfully.(e.g *Jobs/CronJobs*)  
**Failed** - All containers in the pod have terminated and at least one of them with non-zero status.  
**Unknown** - state of ht epod can't be retrieved.(e.g *Interaction error between master and node*)  
**CrashLoopBackOff** - The container failed to start and is tried again and again.(e.g *Container is set with wrong params*)  
#### Deployment strategies
![alt text](https://blog.container-solutions.com/hs-fs/hubfs/Blog%20Images/K8s_deployment_strategies-1.png?width=1396&name=K8s_deployment_strategies-1.png)
#### VPC native network

#### Tags/Metadata/Labels
**Tags** can provide additional functionality in easy way(e.g http-server tag on gcp thath allows traffic on port 80 instead configuring rules for that).  
**Labels** used to organize resources and provide transparency in used resources. It can be handy for billing reports(e.g how much consumes /dev/test/prod).  
**Metadata** provide additionall information to the object from external resources instead of storing extra information in the object(e.g startup script/ssh keys/machine-type).  
#### Liveness probe
**Liveness probe** is ised to identify whether node shold be restarted.(e.g the application isn't responding)
#### Readiness probe
**Readiness probe** is used to identify whether container ready to accept traffic.
#### Taints and Toleration
**Taints** is a functionality that provides you an ability to run only specific pods. That have toleration to all constrints(taints) provided by nodes.
#### Node/Pod Anti/Affinity
**Affinity** is an extended way(comparing to NodeSelector) to attach pods to the nodes with specific labels. Affinity extends NodeSelector functionality as follows:  
1. Avaliability of soft(prefer)/hard(require) limits.  
2. Restrict pod running, based on pod labels currently running on the node(Pod AntiAffinity).  
3. More operators introduced(In,NotIn.NotExist and etc).  

**Node anti/affinity** - restrict or run on nodes with a specific label.  
**Pod anti/affinity** - restrict or run a pod on the node that already running pods with specific labels.  
#### Self-evict annotation
*cluster-autoscaler.kubernetes.io/safe-to-evict* prevents scheduler to move pod to another node in order to reduce cluster size(Cluster Autoscaller).
#### PodDisruptionBudget
**PodDisruptionBudget** is a functionaluty that ensures that specified number of pods currently running in the cluster. It can prevent downscalling or rebooting of the node if minAvailable value is not complied. 
#### Binary Authorization 
**Binary Authorization** kubernetes feature which ensures thaht only trusted images deployed in GKE cluster.This can be done by specifying trusted container registres or images that passed attesting(manually checking that image corresponded to specific requirments and image will be signed with pgp key).   