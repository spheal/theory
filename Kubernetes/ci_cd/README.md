## Basics
#### Continuous Integration
*Continuous Integration* is the process of automating the build and testing of code every time a team member commits changes to version control.
#### Continuous Delivery
*Continuous Delivery* is the process to build, test, configure and deploy from a build to a production environment.
#### Continuous Deployment 
*Continuous Deployment* is the automated process of building,testing and deploying of code to production environment. 
## Benefits of using CI/CD

## Examples
#### General pipeline
![alt text](https://dzone.com/storage/temp/8260021-ci-cd-jenkins-helm-k8s.png)
![alt text](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_extended_11_11.png)
#### Mobile pipeline
![Alt text](./Capture.PNG)  

## Types of tests
![alt text](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2019/02/types-528x297.png)  
*Functional tests* is about  how internal components work and intercat with each other.  
*Non-Functional tests* is about defining characteristics of your software application.
#### Unit tests
**Unit tests** consist in testing methods/functions/classes/modules of your software.
#### Integration tests
**Integration tests** verify that different modules/services of your application work well together.  
*Example: Interaction of web application with database.*
#### Perfomance tests
**Perfomance tests** check the behaviour of the system when it's under significant load. Find bottlenecks for improving your application.  
*Example: Send multiple http requests to define peak load of your application and when it should be autoscale.*
#### UI tests
**UI tests** reffer to checking the functions of an application that are visible to a user.  
*Example: when you click 'Log in' button in your site you will be logged in or asked for authenticating.*
#### Regressions tests
**Regressions tests** ensure that new code/functionality have detrimental no side effects to current application.  
#### Smoke tests
**Smoke tetst** ensure that basic functionality works after adding new changes to the code.
#### Security tests
**Security tests** intended to uncover vulnerabilities of the system/application.


