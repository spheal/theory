## Commiting  
**git commit**: write a commit in the editor set default for git.   
**git commit -am "Message"**: add and commit all modified files.  
## Reset file changes  
#### Filesystem
**git checkout -- 'filename'**: reset file state and its content to a state in the last commit. 
**git clean -xdf**: remove all new files and directories.  
#### Staging 
**git reset HEAD 'filename'**: reset file state from commited to modified.  
#### Commit in local branch
**git reset HEAD~2**: undo last 2 local commits.
  * **git reset --hard**: undo and remove changes according to last commit.   
  * **git reset --mixed**: undo changes to file system.  
  * **git reset --soft**: undo changes to staging.  
**git commit --amend -m "Commit message"**: update last local commit.  
#### Commit in remote repo
**git revert "sha1"**: undo specified commit. Git creates new commit where delete everything that was created in specified commit.  
## Rename file
**git mv 'old_name' 'new_name'**: rename file in staging area and in OS too. 
## Remove file
**git rm 'file_name'**: remove file from repo and from OS too.  
## Merge
Working with targeted branch(master<-feature: need to work with master).  
#### Fast forward merge
Source commits will be added to targeted branch without any new commits.HEAD will be moved to the latest commit in source branch.  
**git merge feeature**: merge feature branch in master.
#### Non fast forward merge  
If targeted branch has new commit git will create new commit where it will unite commits from source branch and then from tageted one.  
#### Rebase
If source branch(from which outgrowth was made) has new commits, we can include these changes by changing commit from which outgrowth will be start.  
**git rebase master feature**   
![alt text](https://jeffkreeftmeijer.com/git-rebase/git-rebase.png)  
#### Cherry-pick
If we want to include one or several commits from any other branch to source one we need to use cherry-pick.  
**git cherry-pick 'commit-hash'**
## Temporary storage of changes
**git stash save "Description"**: place all changes in temprorary storage. This will revert changes and you can apply them later. This can be helpfull when you need to switch to another branch but you code is not ready to be pushed.  
**git stash pop/apply 'stash_name'**: pop will apply stash and remove it.apply will apply stash and keep it.  
**git stash drop 'stash_name'**: remove specified stash.  